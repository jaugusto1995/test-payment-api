﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace API_Pagamentos.Migrations
{
    public partial class AddMudancaBd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "Venda",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Venda_ItemId",
                table: "Venda",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Venda_Item_ItemId",
                table: "Venda",
                column: "ItemId",
                principalTable: "Item",
                principalColumn: "ItemId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Venda_Item_ItemId",
                table: "Venda");

            migrationBuilder.DropIndex(
                name: "IX_Venda_ItemId",
                table: "Venda");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "Venda");
        }
    }
}
